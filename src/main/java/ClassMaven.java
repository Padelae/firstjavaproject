/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pjade
 */
public class ClassMaven implements CommandHandler {
    
    private String statement;
    private String name;
    
    
    //gets the name of the user
    public String getName(String userIn){
        name = userIn;
        return name;
    }
    //used to find the emotion that the user is feeling
    public int getEmo(String userIn){
        
        int s = 0;
        
        for(int i = userIn.length(); i > 0; i--){
        
            if(userIn.toLowerCase().substring(i-4, i).equals("i am")){
                s = i;
                break;
                }
            }
        return s;
    }


    
    //removes any character that is not in the alphabet but keeps spaces
    public void removeChar(String userIn){
        //char[] chars = userIn.toCharArray();
        userIn.trim();
        String newInput = "";
        
        //loops through the string and adds each character to newInput
        for(int i = 0; i < userIn.length()-1; i++){
            newInput += userIn.charAt(i);
        }
        
        newInput.trim();
        userIn = newInput;
            //checks to make sure that output matches initial input

    }

    public String getResponse(String userIn){
        statement = "";
        if(!userIn.equals("") || !userIn.equals(" ")){
            statement = "Sorry, I don't know how to do that.";
        }
        //greeting response
        if(userIn.toLowerCase().equals("hello") ||
                userIn.toLowerCase().equals("hi")){
            statement = "\nHello. You can ask me any question (not really"
                    + "though).";
        }
        //sample questions
        if(userIn.toLowerCase().equals("q")){
            statement = "\nHere are questions I am limited to at this moment: \n"
                    + "\nHow are you? \nDo you need help? \nWhat is the superior"
                    + " console?" + "\nI can also do simple math problems like "
                    + "1+1, 2*3, 4^1, or 9/3.";
        }
        
        //response to how are you
        if(userIn.toLowerCase().equals("how are you")){
            statement = "\nI am a computer program. But I feel great! \nHow are "
                    + "you?";
        }
        
        if(userIn.toLowerCase().contains("i am")){            
            /**statement = "Why do you feel" + userIn.substring(userIn.indexOf
             * ("i am") + 4);
             **/
            statement = "Why do you feel" + 
                    userIn.substring(getEmo(userIn));
        }
        
        if(userIn.toLowerCase().contains("what is ")){
            if(userIn.toLowerCase().contains("the")){
            statement = "PS4 of course!";
        }
            
        }
        
        if(userIn.toLowerCase().equals("do you need help")){
            statement = "Why would I need help? I'm here for you!";
        }
        
        if(userIn.toLowerCase().equals("thank you") || userIn.toLowerCase().
                equals("thanks")){
            statement = "You're welcome :)";
        }

        if(userIn.toLowerCase().equals("idk")){
            statement = "Ok.";
        }
        return statement;
    }

    //implemented handler to check if user input can be handled by file
    @Override
    public boolean canProcess(String command) {
        return true;
    }

    //runs code if canProcess returns true
    @Override
    public String processCommand(String command) {
        return getResponse(command);
    }
    }

