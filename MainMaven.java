import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//import ClassMaven;
/**
 *
 * @author pjade
 */
public class MainMaven {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList <CommandHandler> handlers = new ArrayList();
        handlers.add(new MathMaven());
        handlers.add(new ClassMaven());


        // TODO code application logic here
        
        Scanner scan = new Scanner (System.in);
        
       
        
        String statement = scan.nextLine();
        
        //first.greeting();
        
        for(CommandHandler handler : handlers){
            
            if(handler.canProcess(statement)){
                System.out.println(handler.processCommand(statement));
                break;
            }

        }    
        
            statement = scan.nextLine();
            
            
        }
        
        
    }
    

